package com.bektursun.covid19.utils

import android.app.Application
import com.bektursun.covid19.ui.detailRoute.fragment.DetailRouteSheetViewModelFactory

object InjectorUtils {

    fun provideRouteSheetDetailViewModelFactory(
        application: Application,
        routeSheetId: Int
    ): DetailRouteSheetViewModelFactory {

        return DetailRouteSheetViewModelFactory(routeSheetId, application)
    }
}