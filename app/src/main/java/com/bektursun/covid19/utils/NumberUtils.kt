package com.bektursun.covid19.utils

import android.os.Build
import java.text.NumberFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object NumberUtils {

    fun formatTime(time: String): String {
        val instant = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Instant.parse(time)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val local = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            instant.atZone(ZoneId.systemDefault()).toLocalDateTime()
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        return local.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))
    }

    fun numberFormat(number: Int?) = number?.let {
        NumberFormat.getNumberInstance(Locale.getDefault()).format(number)
    } ?: "-"

    fun extractDigit(number: String) = Regex("[^0-9]").replace(number, "").toInt()
}