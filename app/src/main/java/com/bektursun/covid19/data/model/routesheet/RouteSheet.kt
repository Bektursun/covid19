package com.bektursun.covid19.data.model.routesheet

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "route_table")
@Parcelize
data class RouteSheet(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "full_name")
    val fullName: String,
    @ColumnInfo(name = "phone_number")
    val phoneNumber: String,
    @ColumnInfo(name = "reason_to_leave")
    val reasonToLeaveHome: String,
    @ColumnInfo(name = "exit_time")
    val exitTime: String,
    @ColumnInfo(name = "residence_address")
    val residenceAddress: String,
    @ColumnInfo(name = "destination")
    val destination: String
): Parcelable