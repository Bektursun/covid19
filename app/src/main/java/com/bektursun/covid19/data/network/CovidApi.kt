package com.bektursun.covid19.data.network

import com.bektursun.covid19.data.model.KgData
import com.bektursun.covid19.data.model.covid.CovidOverview
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface CovidApi {

    @GET("api/countries/{country}")
    fun getStatisticForKG(@Path("country") country: String="kyrgyzstan"): Observable<KgData>

    @GET("api")
    fun getAllStatistic(): Observable<CovidOverview>
}