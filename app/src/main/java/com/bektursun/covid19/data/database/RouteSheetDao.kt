package com.bektursun.covid19.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bektursun.covid19.data.model.routesheet.RouteSheet

@Dao
interface RouteSheetDao {

    @Query("SELECT * FROM route_table ORDER BY id DESC")
    fun getAllRouteSheet(): LiveData<List<RouteSheet>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertRouteSheet(routeSheet: RouteSheet)

    @Query("DELETE FROM route_table")
    suspend fun deleteAllRouteSheet()

    @Query("SELECT * FROM route_table WHERE id = :routeId")
    fun getRouteSheetById(routeId: Int): LiveData<RouteSheet>

    @Query("DELETE FROM route_table WHERE id = :routeId")
    suspend fun deleteRouteSheetById(routeId: Int)

}