package com.bektursun.covid19.data.model


import com.squareup.moshi.Json

data class KgData(
    @Json(name = "confirmed")
    val confirmed: Confirmed,
    @Json(name = "deaths")
    val deaths: Deaths,
    @Json(name = "lastUpdate")
    val lastUpdate: String,
    @Json(name = "recovered")
    val recovered: Recovered
)