package com.bektursun.covid19.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bektursun.covid19.data.model.routesheet.RouteSheet

@Database(entities = [RouteSheet::class], version = 4, exportSchema = false)
abstract class RouteSheetDatabase : RoomDatabase() {

    abstract fun routeSheetDao(): RouteSheetDao

    companion object {
        @Volatile
        private var INSTANCE: RouteSheetDatabase? = null

        fun getDatabase(context: Context): RouteSheetDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null)
                return tempInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RouteSheetDatabase::class.java,
                    "routeSheet_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                return instance
            }
        }
    }
}