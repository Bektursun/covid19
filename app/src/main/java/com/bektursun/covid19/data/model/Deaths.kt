package com.bektursun.covid19.data.model


import com.squareup.moshi.Json

data class Deaths(
    @Json(name = "detail")
    val detail: String,
    @Json(name = "value")
    val value: Int
)