package com.bektursun.covid19.di.component

import com.bektursun.covid19.ui.statistic.CovidStatisticViewModel
import com.bektursun.covid19.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(covidStatisticViewModel: CovidStatisticViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}