package com.bektursun.covid19.base

import androidx.lifecycle.ViewModel
import com.bektursun.covid19.ui.statistic.CovidStatisticViewModel
import com.bektursun.covid19.di.module.NetworkModule
import com.bektursun.covid19.di.component.DaggerViewModelInjector
import com.bektursun.covid19.di.component.ViewModelInjector

abstract class BaseViewModel : ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is CovidStatisticViewModel -> injector.inject(this)
        }
    }
}