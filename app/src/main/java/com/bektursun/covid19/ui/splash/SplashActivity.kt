package com.bektursun.covid19.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.bektursun.covid19.R
import com.bektursun.covid19.ui.MainActivity
import com.bektursun.covid19.utils.postDelayed
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    private lateinit var sideAnim: Animation
    private lateinit var bottomAnim: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        postDelayed(SPLASH_TIMER) {
            task()
        }
    }

    override fun onStart() {
        super.onStart()
        sideAnim = AnimationUtils.loadAnimation(this, R.anim.side_anim)
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_anim)
        background_image.animation = sideAnim
        powered_by.animation = bottomAnim
    }

    private fun task() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private const val SPLASH_TIMER: Long = 3000
    }
}
