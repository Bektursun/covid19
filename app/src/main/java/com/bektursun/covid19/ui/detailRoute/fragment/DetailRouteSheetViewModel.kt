package com.bektursun.covid19.ui.detailRoute.fragment

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bektursun.covid19.data.database.RouteSheetDatabase
import com.bektursun.covid19.repository.RouteSheetRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailRouteSheetViewModel(application: Application, routeSheetId: Int) : ViewModel() {

    private val routeSheetRepository: RouteSheetRepository

    private val _isRouteDelete = MutableLiveData<Boolean>()
    val isRouteDelete: LiveData<Boolean>
        get() = _isRouteDelete

    private val routeId: Int = routeSheetId

    init {
        val routeSheetDao = RouteSheetDatabase.getDatabase(application).routeSheetDao()
        routeSheetRepository = RouteSheetRepository(routeSheetDao)
    }

    val routeSheet = routeSheetRepository.getRouteSheetById(routeSheetId)

    fun deleteRouteSheetById() = viewModelScope.launch(Dispatchers.IO) {
        routeSheetRepository.deleteRouteSheetById(routeId)
        _isRouteDelete.postValue(true)
    }
}
