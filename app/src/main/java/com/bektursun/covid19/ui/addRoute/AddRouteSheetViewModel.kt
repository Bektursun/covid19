package com.bektursun.covid19.ui.addRoute

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bektursun.covid19.data.database.RouteSheetDatabase
import com.bektursun.covid19.data.model.routesheet.RouteSheet
import com.bektursun.covid19.repository.RouteSheetRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddRouteSheetViewModel(application: Application) : ViewModel() {

    private val routeSheetRepository: RouteSheetRepository

    init {
        val routeSheetDao = RouteSheetDatabase.getDatabase(application).routeSheetDao()
        routeSheetRepository = RouteSheetRepository(routeSheetDao)
    }

    fun insertRouteSheet(routeSheet: RouteSheet) = viewModelScope.launch(Dispatchers.IO) {
        routeSheetRepository.insert(routeSheet)
    }
}
