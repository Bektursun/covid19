package com.bektursun.covid19.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.bektursun.covid19.databinding.FragmentMainBinding
import com.bektursun.covid19.ui.adapter.RouteSheetListAdapter


class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private lateinit var viewModel: RouteSheetViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(RouteSheetViewModel::class.java)
        val adapter = RouteSheetListAdapter()
        binding.routeRV.adapter = adapter
        subscribeUI(adapter)

        binding.fab.setOnClickListener {
            val direction = MainFragmentDirections.actionMainFragmentToAddRouteSheetFragment()
            findNavController().navigate(direction)
        }

        return binding.root
    }

    private fun subscribeUI(adapter: RouteSheetListAdapter) {
        viewModel.allRouteSheets.observe(viewLifecycleOwner) { routes ->
            adapter.submitList(routes)
        }
    }
}
