package com.bektursun.covid19.ui.detailRoute.fragment

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DetailRouteSheetViewModelFactory(
    private val routeSheetId: Int,
    private val application: Application
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailRouteSheetViewModel(application, routeSheetId) as T
    }
}