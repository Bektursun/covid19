package com.bektursun.covid19.ui.statistic

import android.animation.ValueAnimator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bektursun.covid19.R
import com.bektursun.covid19.data.model.KgData
import com.bektursun.covid19.data.model.covid.CovidOverview
import com.bektursun.covid19.databinding.FragmentCovidStatisticBinding
import com.bektursun.covid19.utils.NumberUtils
import com.bektursun.covid19.utils.color
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.google.android.material.snackbar.Snackbar
import com.iammert.library.ui.multisearchviewlib.MultiSearchView


class CovidStatisticFragment : Fragment() {

    private lateinit var binding: FragmentCovidStatisticBinding
    private lateinit var viewModel: CovidStatisticViewModel

    private var recovered: Int = 0
    private var deaths: Int = 0
    private var active: Int = 0
    private var confirmed: Int = 0

    private var errorSnackbar: Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            FragmentCovidStatisticBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(CovidStatisticViewModel::class.java)

        viewModel.kgData.observe(viewLifecycleOwner, Observer {
            bindDataForKG(it)
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage)
            else hideError()
        })

        viewModel.allStats.observe(viewLifecycleOwner, Observer {
            bindAllStats(it)
        })

        viewModel.countryName.observe(viewLifecycleOwner, Observer {
            binding.countryNameTV.text = it
        })

        search()
        // https://developer.android.com/topic/libraries/data-binding/expressions

        binding.viewModel = viewModel

        return binding.root
    }

    private fun search() {
        binding.multiSearchView.setSearchViewListener(object :
            MultiSearchView.MultiSearchViewListener {
            override fun onItemSelected(index: Int, s: CharSequence) {
                Log.d("SearchSelected", "${index}, $s")
                viewModel.searchDataByCountryName(s.toString())
            }

            override fun onSearchComplete(index: Int, s: CharSequence) {
                Log.d("SearchComplete", "$index, $s")
                viewModel.searchDataByCountryName(s.toString())
            }

            override fun onSearchItemRemoved(index: Int) {
                Log.d("SearchRemoved", "$index")
            }

            override fun onTextChanged(index: Int, s: CharSequence) {
                Log.d("SearchChanged", "$index, $s")
            }
        })
    }

    private fun bindAllStats(data: CovidOverview) {
        active = data.confirmed!!.value.minus(data.recovered!!.value).minus(data.deaths!!.value)
        confirmed = data.confirmed.value
        deaths = data.deaths.value
        recovered = data.recovered.value

        with(binding) {
            startNumberChangeAnimator(active, txtActive)
            startNumberChangeAnimator(deaths, txtDeaths)
            startNumberChangeAnimator(recovered, txtRecovered)
            startNumberChangeAnimator(confirmed, txtCases)
        }
        setDataToChart()
    }

    private fun bindDataForKG(data: KgData) {
        val confirmed: MutableLiveData<Int> = MutableLiveData(data.confirmed.value)
        val recovered: MutableLiveData<Int> = MutableLiveData(data.recovered.value)
        val deaths: MutableLiveData<Int> = MutableLiveData(data.deaths.value)
        val lastUpdate: MutableLiveData<String> = MutableLiveData(data.lastUpdate)

        with(binding) {
            confirmedContent.text = confirmed.value.toString()
            recoveredContent.text = recovered.value.toString()
            deathContent.text = deaths.value.toString()
            lastUpdateContent.text =
                getString(R.string.last_update, NumberUtils.formatTime(lastUpdate.value.toString()))
        }
    }

    private fun showError(errorMessage: String) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_SHORT)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

    private fun setDataToChart() {
        val pieDataSet = PieDataSet(
            listOf(
                PieEntry(active.toFloat(), getString(R.string.active)),
                PieEntry(recovered.toFloat(), getString(R.string.recovered)),
                PieEntry(deaths.toFloat(), getString(R.string.deaths))
            ), getString(R.string.covid_19)
        )

        with(binding.root.context) {
            val colors = arrayListOf(
                color(R.color.color_active),
                color(R.color.color_recovered),
                color(R.color.red)
            )
            pieDataSet.colors = colors
        }
        val pieData = PieData(pieDataSet)
        pieData.setDrawValues(false)
        with(binding.pieChart) {
            if (data == pieData) return
            data = pieData
            legend.isEnabled = false
            description = null
            holeRadius = PIE_RADIUS
            setHoleColor(context.color(R.color.cinder_grey))
            setDrawEntryLabels(false)
            animateY(PIE_ANIMATION_DURATION, Easing.EaseInOutQuart)
            invalidate()
            binding.layoutCases.visibility = View.VISIBLE
        }
    }

    private fun startNumberChangeAnimator(finalValue: Int?, view: TextView) {
        val initialValue = NumberUtils.extractDigit(view.text.toString())
        val valueAnimator = ValueAnimator.ofInt(initialValue, finalValue ?: 0)
        valueAnimator.duration = TEXT_ANIMATION_DURATION
        valueAnimator.addUpdateListener { value ->
            view.text = NumberUtils.numberFormat(value.animatedValue.toString().toInt())
        }
        valueAnimator.start()
    }

    companion object {
        private const val TEXT_ANIMATION_DURATION = 1000L
        private const val PIE_ANIMATION_DURATION = 1500
        private const val PIE_RADIUS = 75f
    }
}
