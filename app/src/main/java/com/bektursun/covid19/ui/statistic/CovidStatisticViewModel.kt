package com.bektursun.covid19.ui.statistic

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bektursun.covid19.base.BaseViewModel
import com.bektursun.covid19.data.model.KgData
import com.bektursun.covid19.data.model.covid.CovidOverview
import com.bektursun.covid19.data.network.CovidApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CovidStatisticViewModel : BaseViewModel() {

    @Inject
    lateinit var covidApi: CovidApi

    private lateinit var subscription: Disposable

    private val _allStats: MutableLiveData<CovidOverview> = MutableLiveData()
    val allStats: LiveData<CovidOverview>
        get() = _allStats

    private val _countryName: MutableLiveData<String> = MutableLiveData()
    val countryName: LiveData<String>
        get() = _countryName

    private val _kgData: MutableLiveData<KgData> = MutableLiveData()
    val kgData: LiveData<KgData>
        get() = _kgData

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val errorClickListener = View.OnClickListener { getStatsForKg() }

    init {
        getGlobalStatistic()
        getStatsForKg()
    }

    private fun getGlobalStatistic() {
        subscription = covidApi.getAllStatistic()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onRetrieveCovidStart() }
            .doOnTerminate { onRetrieveCovidFinish() }
            .subscribe(
                { result -> onRetrieveWorldTotalStatistic(result) },
                { error -> onRetrieveCovidError(error.message) })
    }

    private fun getStatsForKg() {
        subscription = covidApi.getStatisticForKG()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onRetrieveCovidStart() }
            .doOnTerminate { onRetrieveCovidFinish() }
            .subscribe({ result -> setKgData(result) },
                { onRetrieveCovidError(it.message) })
    }

    fun searchDataByCountryName(country: String) {
        subscription = covidApi.getStatisticForKG(country)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveCovidStart() }
            .doOnTerminate { onRetrieveCovidFinish() }
            .subscribe({ result -> setKgData(result) },
                { onRetrieveCovidError(it.message) })

        _countryName.postValue(country)
    }

    private fun setKgData(data: KgData) {
        _kgData.postValue(data)
    }

    private fun onRetrieveWorldTotalStatistic(data: CovidOverview) {
        _allStats.postValue(data)
    }

    private fun onRetrieveCovidStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveCovidFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveCovidError(error: String?) {
        errorMessage.value = error
        _countryName.postValue(null)
        Log.d("ERROR", error!!)
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}