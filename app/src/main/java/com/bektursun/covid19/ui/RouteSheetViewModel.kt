package com.bektursun.covid19.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.bektursun.covid19.data.database.RouteSheetDatabase
import com.bektursun.covid19.data.model.routesheet.RouteSheet
import com.bektursun.covid19.repository.RouteSheetRepository

class RouteSheetViewModel(application: Application) : AndroidViewModel(application) {

    private val routeSheetRepository: RouteSheetRepository

    val allRouteSheets: LiveData<List<RouteSheet>>

    init {
        val routeSheetDao = RouteSheetDatabase.getDatabase(application).routeSheetDao()
        routeSheetRepository = RouteSheetRepository(routeSheetDao)
        allRouteSheets = routeSheetRepository.allRoutes()
    }
}