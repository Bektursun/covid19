package com.bektursun.covid19.ui.addRoute

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bektursun.covid19.R
import com.bektursun.covid19.data.model.routesheet.RouteSheet
import com.bektursun.covid19.databinding.AddRouteSheetFragmentBinding
import com.bektursun.covid19.utils.Utils
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class AddRouteSheetFragment : Fragment() {

    private lateinit var binding: AddRouteSheetFragmentBinding
    private lateinit var viewModel: AddRouteSheetViewModel

    private lateinit var textInputLayouts: List<TextInputLayout>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.add_route_sheet_fragment, container, false)

        viewModel = AddRouteSheetViewModel(activity!!.application)

        binding.addRouteSheetButton.setOnClickListener {
            newRouteValidation()
        }
        textInputLayouts = Utils.findViewsWithType(binding.root, TextInputLayout::class.java)

        return binding.root
    }

    private fun newRouteValidation() {
        var noErrors = true
        for (textInputLayout in textInputLayouts) {
            val editTextString = textInputLayout.editText!!.text.toString()
            if (editTextString.isEmpty()) {
                textInputLayout.error = resources.getString(R.string.error_string)
                noErrors = false
            } else textInputLayout.error = null
        }
        if (noErrors) {
            insertNewRouteSheet()
            Snackbar.make(binding.root, getString(R.string.success), Snackbar.LENGTH_SHORT).show()
            val direction =
                AddRouteSheetFragmentDirections.actionAddRouteSheetFragmentToMainFragment()
            findNavController().navigate(direction)
        }
    }

    private fun insertNewRouteSheet() {
        val routeSheet = RouteSheet(
            date = binding.dateContent.text.toString(),
            fullName = binding.fullNameContent.text.toString(),
            phoneNumber = "996" + binding.phoneNumberContent.text.toString(),
            reasonToLeaveHome = binding.reasonToLeaveContent.text.toString(),
            exitTime = binding.exitTimeContent.text.toString(),
            residenceAddress = binding.residenceAddressContent.text.toString(),
            destination = binding.destinationContent.text.toString()
        )

        viewModel.insertRouteSheet(routeSheet)
    }
}
