package com.bektursun.covid19.ui.detailRoute.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bektursun.covid19.R
import com.bektursun.covid19.databinding.DetailRouteSheetFragmentBinding
import com.bektursun.covid19.utils.InjectorUtils
import com.google.android.material.snackbar.Snackbar

class DetailRouteSheetFragment : Fragment() {

    private lateinit var binding: DetailRouteSheetFragmentBinding
    private val args: DetailRouteSheetFragmentArgs by navArgs()
    private val routeViewModel: DetailRouteSheetViewModel by viewModels {
        InjectorUtils.provideRouteSheetDetailViewModelFactory(activity?.application!!, args.routeId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.detail_route_sheet_fragment,
            container,
            false
        )

        binding.apply {
            viewModel = routeViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        routeViewModel.isRouteDelete.observe(viewLifecycleOwner, Observer { isDeleted ->
            if (isDeleted == true) {
                Snackbar.make(
                    binding.root,
                    getString(R.string.success_deleted),
                    Snackbar.LENGTH_SHORT
                ).show()
                val direction =
                    DetailRouteSheetFragmentDirections.actionDetailRouteSheetFragmentToMainFragment()
                findNavController().navigate(direction)
            } else
                return@Observer
        })

        return binding.root
    }
}
