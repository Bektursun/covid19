package com.bektursun.covid19.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.covid19.data.model.routesheet.RouteSheet
import com.bektursun.covid19.databinding.RouteItemBinding
import com.bektursun.covid19.ui.MainFragmentDirections

class RouteSheetListAdapter : ListAdapter<RouteSheet, RouteSheetListAdapter.ViewHolder>(
    RouteSheetDiffCallback()
) {

    class ViewHolder(private val binding: RouteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.route?.let { routeSheet ->
                    navigateToRoute(routeSheet, it)
                }
            }
        }

        private fun navigateToRoute(routeSheet: RouteSheet, view: View) {
            val direction =
                MainFragmentDirections.actionMainFragmentToDetailRouteSheetFragment(routeSheet.id!!)
            Toast.makeText(view.context, "${routeSheet.id}", Toast.LENGTH_SHORT).show()
            view.findNavController().navigate(direction)
        }

        fun bind(item: RouteSheet) {
            binding.apply {
                route = item
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RouteItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val routeSheet = getItem(position)
        holder.bind(routeSheet)
    }
}

private class RouteSheetDiffCallback : DiffUtil.ItemCallback<RouteSheet>() {

    override fun areItemsTheSame(oldItem: RouteSheet, newItem: RouteSheet): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: RouteSheet, newItem: RouteSheet): Boolean {
        return oldItem == newItem
    }
}