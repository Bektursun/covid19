package com.bektursun.covid19.repository

import com.bektursun.covid19.data.database.RouteSheetDao
import com.bektursun.covid19.data.model.routesheet.RouteSheet

class RouteSheetRepository(private val routeSheetDao: RouteSheetDao) {

    fun allRoutes() = routeSheetDao.getAllRouteSheet()

    fun getRouteSheetById(routeId: Int) = routeSheetDao.getRouteSheetById(routeId)

    suspend fun deleteRouteSheetById(routeId: Int) = routeSheetDao.deleteRouteSheetById(routeId)

    suspend fun insert(routeSheet: RouteSheet) = routeSheetDao.insertRouteSheet(routeSheet)
}