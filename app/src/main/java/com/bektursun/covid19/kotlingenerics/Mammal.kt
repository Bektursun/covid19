package com.bektursun.covid19.kotlingenerics

import java.util.*
import java.util.function.Function
import kotlin.random.Random
import kotlin.reflect.KFunction
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

sealed class Mammal1(val name: String) {
    fun eat() {}
    fun sleep() {}
    fun swim() {
        println("${name.toUpperCase(Locale.ROOT)} Can swim")
    }

    open fun relief() {}
}

data class Sloths(
    val slothName: String,
    val isTwoFingered: Boolean,
    var slothWeight: Int
) : Mammal1(slothName) {

    override fun relief() {
        val oldWeight = slothWeight
        val weightShed = Random.nextInt(0, slothWeight / 3)
        val newWeight = slothWeight - weightShed
        println("${slothName.toUpperCase(Locale.getDefault())} Finally went this week")
        println("\tOld weight: $oldWeight \t|\t New weight: $newWeight")
    }
}

data class Manatee(val manateeName: String) : Mammal1(manateeName)
data class Pandas(val pandasName: String) : Mammal1(pandasName)

fun slothActivity(sloths: Sloths, action: Unit) {
    sloths.run {
        action
    }
}

fun main() {

    val slothCrew = listOf(
        Sloths("Bolot", false, 45),
        Sloths("Tima", true, 40),
        Sloths("Nurda", true, 42),
        Manatee("Beka"),
        Pandas("Feka")
    )

    slothCrew.forEach {
        /*mammalFactCheck(it, Mammal1::vertebraeCount)
        mammalFactCheck(it, Mammal1::knownSpeciesCount)*/
    }

}

fun Mammal1.vertebraeCount(): Int {
    return when (this) {
        is Manatee -> 6
        is Sloths -> 9
        else -> 8
    }
}

fun Mammal1.knownSpeciesCount(): Int {
    return when (this) {
        is Pandas -> 2
        is Manatee -> 4
        is Sloths -> 5
    }
}

/*
fun mammalFactCheck(mammal: Mammal1, factCheck: KFunction1<Mammal1, Int>): Int {
    return factCheck(mammal)
}*/
