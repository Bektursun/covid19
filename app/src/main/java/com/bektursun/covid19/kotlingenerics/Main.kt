package com.bektursun.covid19.kotlingenerics

fun main() {

    val emptySloth = listOf<Sloth>()

    val slothCrew = listOf(
        Sloth("Jerry", false),
        Sloth("Bare", true),
        Sloth("Chrissy", true)
    )

    val pandaCrew = listOf(
        Panda("Jay"),
        Panda("Peggy")
    )

    val crewCrewCrew = listOf(
        Sloth("Jerry", false),
        Sloth("Bare", true),
        Sloth("Chrissy", true),
        Panda("Jay"),
        Panda("Peggy")
    )

    feedCrew(slothCrew)
    feedCrew(pandaCrew)

    val compareByName = Comparator { a: Mammal, b: Mammal ->
        a.name.first().toInt() - b.name.first().toInt()
    }
    println(crewCrewCrew.sortedWith(compareByName))
}

private fun feedCrew(crew: List<Mammal>) {
    crew.forEach {
        it.eat()
    }
}
